import React from 'react'
import { render } from 'react-dom'
import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import logger from 'redux-logger'
import thunk from 'redux-thunk'
import { display_products } from './reducers/products.js'
import { getAllProducts } from './actions'
import App from './containers/App'
import ProductsContainer from './containers/ProductsContainer'
import ProductDetail from './components/ProductDetail'
import Router from 'react-router'
import { DefaultRoute, Link, Route } from 'react-router'

const middleware = process.env.NODE_ENV === 'production' ?
  [ thunk ] :
  [ thunk, logger() ]

const createStoreWithMiddleware = applyMiddleware(...middleware)(createStore)
const store = createStoreWithMiddleware(display_products)

store.dispatch(getAllProducts())
const render_component = () => {
	React.render(
              <Provider store={store}>
			    <Router>
			      <Route path="/" component={App}>
			        <Route path="product/:id" component={ProductDetail} />
			      </Route>
			    </Router>
			  </Provider>,
               document.getElementById('root')
                )
    }

store.subscribe(render_component)
render_component();
