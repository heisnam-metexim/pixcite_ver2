import React, { Component, PropTypes } from 'react'

export default class MyGallery extends Component {
  render() {
    const { gallery } = this.props
    return (
      <a style={{paddingLeft:0}} title={"Gallery1"} className="col-xs-6" href={gallery.image} >
                        <img title={"Gallery1"} src={gallery.image}  alt="" />
      </a>
    )
  }
}


