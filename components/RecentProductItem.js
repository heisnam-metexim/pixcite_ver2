import React, { Component, PropTypes } from 'react'
import RecentProduct from './RecentProduct'

export default class RecentProductItem extends Component {
  render() {
    const { product } = this.props

    return (
        <RecentProduct
          title={product.title}
          price={product.price}
          description = {product.description}
          image = {product.image}  />
    )
  }
}

