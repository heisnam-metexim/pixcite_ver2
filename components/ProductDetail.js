import React, { Component, PropTypes } from 'react'
import { Provider } from 'react-redux'
import {List, Map} from 'immutable';

export default class ProductDetail extends Component {
    render() {

        let currentProd  = ""
        let prodId = this.props.params.id
        List(this.context.store.getState().get('items')).map(function (product, i) {
            if(product.id == prodId){ currentProd =  product }
        })
        return (
            <div className ="container">
                <div className="col-md-9">
                    <div className="thumbnail">
                        <img className="img-responsive" src={currentProd.image} alt="" />
                        <div className="caption-full">
                            <h4>{currentProd.title}</h4>
                            <h4>Title:&#36;{currentProd.price}</h4>
                            <p>{currentProd.description}</p>
                            <p>{currentProd.details}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}


ProductDetail.contextTypes = {
  store: React.PropTypes.object
}
