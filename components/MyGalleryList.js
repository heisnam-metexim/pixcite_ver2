import React, { Component, PropTypes } from 'react'

export default class MyGalleryList extends Component {
  render() {
    return (
      <div>
        <div>{this.props.children}</div>
      </div>
    )
  }
}