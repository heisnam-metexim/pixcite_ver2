import React, { Component, PropTypes } from 'react'
import { Router, Route, Link } from 'react-router'


export default class RecentProduct extends Component {
  render() {
    const {id, price, quantity, title, description, image } = this.props
    return <div className="col-xs-6" style={{ paddingLeft: 0 }}>
            <div className="thumbnail">
              <img src={image}/>
              <div className="caption">
                <h4 className="pull-right">&#36;{price}</h4>
                <h4>{title}</h4>
                  <p>{description}</p>
              </div>
            </div>
           </div>
  }
}


