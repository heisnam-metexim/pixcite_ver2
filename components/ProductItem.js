import React, { Component, PropTypes } from 'react'
import Product from './Product'

export default class ProductItem extends Component {
  render() {
    const { product } = this.props

    return (
      <div
        style={{ marginBottom: 40 }}>
        <Product
          id   ={product.id}
          title={product.title}
          price={product.price}
          description = {product.description}
          image = {product.image}  />
      </div>
    )
  }
}

