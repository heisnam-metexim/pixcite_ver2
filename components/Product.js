import React, { Component, PropTypes } from 'react'
import { Router, Route, Link } from 'react-router'


export default class Product extends Component {
  render() {
    const {id, price, quantity, title, description, image } = this.props 
    return(<Link to={`/product/${id}`}>
            <div className="col-sm-4 col-lg-4 col-md-4">
                <div className="thumbnail">
                  <img src={image}/>
                  <div className="caption">
                    <h4 className="pull-right">&#36;{price}</h4>
                    <h4><Link to={'/product/${id}'}>{title}</Link></h4>
                      <p>{description}</p>
                  </div>
                </div>
            </div>
          </Link>  
    )
  }
}


