import {expect} from 'chai';
import { display_products } from '../reducers/products'
import {List, Map} from 'immutable';
import _products from '../api/products.json'

describe('display_products', () => {

	 it('returns the products', () => {
      let current_state = Map({});
     
      let action = {
                      type: 'RECEIVE_PRODUCTS',
                      products: _products
                    };            
      let nextState = display_products(current_state, action);
      console.log(JSON.stringify(nextState.keys()));
      expect(nextState).to.have.property('items').with.length(3);
    });

})
