import React, { Component, PropTypes } from 'react'


export default class Slider extends Component {
  render() {
    return (
        <div className="col-md-12">
            <div data-ride="carousel" className="carousel slide" id="carousel-example-generic">
                <ol className="carousel-indicators">
                    <li className="active" data-slide-to="0" data-target="#carousel-example-generic"></li>
                    <li data-slide-to="1" data-target="#carousel-example-generic" className=""></li>
                    <li data-slide-to="2" data-target="#carousel-example-generic" className=""></li>
                </ol>
                <div className="carousel-inner">
                    <div className="item active">
                        <img alt="" src="images/banner2.jpg" className="slide-image" />
                    </div>
                    <div className="item">
                        <img alt="" src="images/banner3.jpg" className="slide-image" />
                    </div>
                    <div className="item">
                        <img alt="" src="images/banner2.jpg" className="slide-image" />
                    </div>
                </div>
                <a data-slide="prev" href="#carousel-example-generic" className="left carousel-control">
                    <span className="glyphicon glyphicon-chevron-left"></span>
                </a>
                <a data-slide="next" href="#carousel-example-generic" className="right carousel-control">
                    <span className="glyphicon glyphicon-chevron-right"></span>
                </a>
            </div>
        </div>
    )
  }
}
