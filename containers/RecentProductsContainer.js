import React, { Component, PropTypes } from 'react'
import RecentProductItem from '../components/RecentProductItem'
import RecentProductsList from '../components/RecentProductsList'
import { Provider } from 'react-redux'
import {List, Map} from 'immutable';

export default class RecentProductsContainer extends Component {
  render() {  
    let  recent_project_items = List(this.context.store.getState().get('recent_project_items'))
    return (
     <RecentProductsList>
        {recent_project_items.map(product =>
          <RecentProductItem
            key={product.id}
            product={product} />
        )}
      </RecentProductsList>
    )
  }
}


RecentProductsContainer.contextTypes = {
  store: React.PropTypes.object
}

