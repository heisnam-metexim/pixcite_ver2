import React, { Component, PropTypes } from 'react'
import ProductsContainer from './ProductsContainer'
import RecentProductsContainer from './RecentProductsContainer'
import MyGalleryContainer from './MyGalleryContainer'
import Slider from './Slider'
import { DefaultRoute, Link, Route } from 'react-router'
import {List, Map} from 'immutable';


export default class App extends Component {
  render() {
    return (
      <div>
        {this.props.children ||  <div className="col-md-9">
          <div className="row carousel-holder">
            <Slider />
          </div>  
          <div className="row">
            <h3>Products</h3>
            <hr/> 
            <ProductsContainer />
          </div>
          <div className="row">
            <hr/> 
            <div className="col-md-6">  
                <h3> My Recent Projects </h3>  
                <RecentProductsContainer />
            </div>
            <div className="col-md-6">
              <h3> My gallery</h3>
              <MyGalleryContainer />
            </div>   
          </div>
        </div> }      
      </div>
     )
  }
}

