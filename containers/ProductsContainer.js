import React, { Component, PropTypes } from 'react'
import ProductItem from '../components/ProductItem'
import ProductsList from '../components/ProductsList'
import { Provider } from 'react-redux'
import {List, Map} from 'immutable';

export default class ProductsContainer extends Component {
  render() {
    //const { store } = this.context;
    //const  state    = store.getState(); 
    const  items = List(this.context.store.getState().get('items'))
   return (
      <ProductsList>
        {items.map(product =>
          <ProductItem
            key={product.id}
            product={product} />
        )}
      </ProductsList>
    )
  }
}


ProductsContainer.contextTypes = {
  store: React.PropTypes.object
}

