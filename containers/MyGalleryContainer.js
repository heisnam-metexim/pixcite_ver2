import React, { Component, PropTypes } from 'react'
import MyGallery from '../components/MyGallery'
import MyGalleryList from '../components/MyGalleryList'
import { Provider } from 'react-redux'
import {List, Map} from 'immutable';

export default class MyGalleryContainer extends Component {
  render() {
    let  galleries = List(this.context.store.getState().get('my_gallery')) ;
  return (
      <MyGalleryList>
        {galleries.map(gallery =>
          <MyGallery
            key={gallery.id}
            gallery={gallery} />
        )}
      </MyGalleryList>
    )
  }
}


MyGalleryContainer.contextTypes = {
  store: React.PropTypes.object
}