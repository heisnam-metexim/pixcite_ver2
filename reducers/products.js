import { combineReducers } from 'redux'
import { RECEIVE_PRODUCTS} from '../constants/ActionTypes'
import {List, Map} from 'immutable';

export function display_products(state = Map({}), action) {
  switch (action.type) {
    case RECEIVE_PRODUCTS:
        return state.merge(Map({
                  items: List(action.products.items),
                  recent_project_items: List(action.products.recent_project_items),
                  my_gallery: List(action.products.my_gallery)

        }))
    default:
      return state
  }
}



